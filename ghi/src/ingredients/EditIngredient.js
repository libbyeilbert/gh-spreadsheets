import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from '../recipes/RecipeContext';
import { useParams } from "react-router-dom";
import Popup from 'reactjs-popup';



function EditIngredient() {
    const { id } = useParams();
    const { fetchIngredients } = useRecipeContext();
    const inputRef = useRef();
    const [name, setName] = useState('');
    const [amount, setAmount] = useState(0);
    const [costPerOz, setCostPerOz] = useState('');
    const [source, setSource] = useState('');
    const [parAmount, setParAmount] = useState('');
    const [parUnit, setParUnit] = useState('');
    const [unit, setUnit] = useState('');
    const [dollarAmount, setDollarAmount] = useState('');


    const handleNameChange = event => {
        setName(event.target.value);
    };

   const handleAmountChange = event => {
        setAmount(event.target.value);
    };
    const handleUnitChange = event => {
        setUnit(event.target.value);
    };

    const handleDollarAmountChange = event => {
        setDollarAmount(event.target.value);
    };

    const handleCostPerOzChange = event => {
        setCostPerOz(event.target.value);
    };
    const handleSourceChange = event => {
        setSource(event.target.value);
    };
    const handleParAmountChange = event => {
        setParAmount(event.target.value);
    };
    const handleParUnitChange = event => {
        setParUnit(event.target.value);
    };

    const calculateCostPerOz = () => {
        if (unit == 'ml') {
            setCostPerOz(dollarAmount / amount * 29.5735)
        } else {
            setCostPerOz(dollarAmount / amount)
        }
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.name = name;
        data.cost_per_oz = parseFloat(costPerOz) || 0;
        data.source = source;
        data.par_amount = parAmount || 0;
        data.par_unit = parUnit;

        const url = `http://localhost:8000/api/ingredients/${id}`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                fetchData();
                await fetchIngredients();
            };
        } catch (error) {
            console.error('Error creating ingredient:', error);
        }
    };
    const fetchData = async () => {
        const recipeUrl = `http://localhost:8000/api/ingredients/${id}`;
        const response = await fetch(recipeUrl);
        if (response.ok) {
            const data = await response.json();
            setName(data.name);
            setCostPerOz(data.cost_per_oz);
            setSource(data.source);
            setParAmount(data.par_amount);
            setParUnit(data.par_unit)
        };
    };
    useEffect(() => {
        inputRef.current.focus();
        fetchData();
    }, []);


    return (
        <>
        <h2>{name}</h2>
        <form onSubmit={handleSubmit} id="new-ingredient-form">
            <label htmlFor="name">Name:</label>
            <input ref={inputRef} value={name} onChange={handleNameChange} placeholder="Name" type="text" id="name" required/>
            <div>
            <label htmlFor="cost-amount">Cost:</label>
            <input value={costPerOz} onChange={handleCostPerOzChange} placeholder="$" type="text" id="cost-amount" />
            per oz

                <Popup trigger={<a>Calculate</a>} position="right center">

                <input value={dollarAmount} onChange={handleDollarAmountChange} type="number" id="dollar-amount" />per
                <input value={amount} onChange={handleAmountChange} type="number" id="amount" />
                <select onChange={handleUnitChange} id="unit">
                    <option value="oz">oz</option>
                    <option value="ml">ml</option>
                </select>
                <button onClick={calculateCostPerOz}>✓</button>
                </Popup>

            </div>
            <label htmlFor="source">Source:</label>
            <input value={source} onChange={handleSourceChange} placeholder="" type="text" id="source" />
            <div>
            <label htmlFor="par-amount">Par:</label>
            <input value={parAmount} onChange={handleParAmountChange} placeholder="Amount" type="text" id="par-amount" />
            <label htmlFor="par-unit"></label>
            <input value={parUnit} onChange={handleParUnitChange} placeholder="unit" type="text" id="par-unit" />
            </div>
            <button>Save</button>
        </form>

        </>
    )
};

export default EditIngredient;
