import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from '../recipes/RecipeContext';
import Popup from 'reactjs-popup';
import styles from './ingredients.module.css';
import jwtDecode from 'jwt-decode';

function Ingredient() {
    const token = localStorage.getItem('token');
    const decodedToken = jwtDecode(token);
    const userId = decodedToken.id;
    const { fetchIngredients, ingredients } = useRecipeContext();
    const inputRef = useRef();
    const [name, setName] = useState('');
    const [source, setSource] = useState('');
    const [parAmount, setParAmount] = useState('');
    const [parUnit, setParUnit] = useState('');
    const [amount, setAmount] = useState(0);
    const [unit, setUnit] = useState('');
    const [costPerOz, setCostPerOz] = useState('');
    const [dollarAmount, setDollarAmount] = useState('');

    const handleNameChange = event => {
        setName(event.target.value);
    };

    const handleSourceChange = event => {
        setSource(event.target.value);
    };
    const handleParAmountChange = event => {
        setParAmount(event.target.value);
    };
    const handleParUnitChange = event => {
        setParUnit(event.target.value);
    };

    const handleAmountChange = event => {
        setAmount(event.target.value);
    };

    const handleUnitChange = event => {
        setUnit(event.target.value);
    };

    const handleDollarAmountChange = event => {
        setDollarAmount(event.target.value);
    };

    const handleCostPerOzChange = event => {
        setCostPerOz(event.target.value);
    };

    const calculateCostPerOz = () => {
        if (unit == 'ml') {
            setCostPerOz(dollarAmount / amount * 29.5735)
        } else {
            setCostPerOz(dollarAmount / amount)
        }
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.name = name;
        data.cost_per_oz = parseFloat(costPerOz) || 0;
        data.source = source;
        data.par_amount = parAmount || 0;
        data.par_unit = parUnit;
        data.user_id = userId;

        const url = 'http://localhost:8000/api/ingredients';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                setName('');
                setDollarAmount('');
                setAmount('');
                setSource('');
                setParAmount('');
                setParUnit('');
                setCostPerOz('');
                inputRef.current.focus();
                await fetchIngredients();
            };
        } catch (error) {
            console.error('Error creating ingredient:', error);
        }
    };

    useEffect(() => {
        inputRef.current.focus();
        fetchIngredients();
    }, []);


    return (
        <>
        <div className={styles.container}>
            <div className={styles.newingredientcontainer}>
        New Ingredient:
        <form onSubmit={handleSubmit} id="new-ingredient-form">
            <label htmlFor="name">Name:</label>
            <input
            style={{ margin: '5px 5px 0px 5px' }}
            ref={inputRef} value={name} onChange={handleNameChange} placeholder="" type="text" id="name" required/>
            <div>
            <label htmlFor="cost-amount">Cost:</label>
            <input
            style={{ margin: '5px 5px', width: '50px' }}
            value={costPerOz} onChange={handleCostPerOzChange} placeholder="$" type="text" id="cost-amount" />
            per oz

                <Popup trigger={<a style={{ margin: '0px 0px 0px 7px', fontSize: '14px' }} >(Calculate)</a>} position="bottom center">

                <input style={{ width: '50px' }} value={dollarAmount} onChange={handleDollarAmountChange} placeholder="$" type="number" id="dollar-amount" />per
                <input style={{ width: '50px' }} value={amount} onChange={handleAmountChange} type="number" id="amount" />
                <select onChange={handleUnitChange} id="unit">
                    <option value="oz">oz</option>
                    <option value="ml">ml</option>
                </select>
                <button onClick={calculateCostPerOz}>✓</button>
                </Popup>

            </div>

            <button type="submit">Create</button>
        </form>
        </div>
        <div className={styles.ingredientlistcontainer}>
        <ul className={styles.ingredientlist}>
            {ingredients.map(ingredient => {
                const ingredientUrl = `http://localhost:3000/ingredients/${ingredient.id}`
                return (
                    <li className={styles.listitem}
                    key={ingredient.id}>
                        <a className={styles.links} href={ingredientUrl}>{ingredient.name}</a>
                    </li>
                )
            })}
        </ul>
        </div>
        </div>
        </>
    )
};

export default Ingredient;
