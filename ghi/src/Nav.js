import { NavLink } from "react-router-dom";
import styles from './nav.module.css';
import { useNavigate } from "react-router-dom";



function Nav() {
    const navigate = useNavigate();
    const isAuthenticated = !!localStorage.getItem('token');


    const logout = async event => {
        event.preventDefault();
        localStorage.setItem('token', '');
        navigate('/');

}
    return (
        <>
        <div className={styles.container}>
            {isAuthenticated && (
            <ul className={styles.navbarlist}>
                <li className={styles.navitem}>
                    <NavLink className={styles.links} to="cocktails">Cocktails</NavLink>
                </li>
                <li className={styles.navitem}>
                    <NavLink className={styles.links} to="ingredients">Ingredients</NavLink>
                </li>
                <li className={styles.navitem}>
                    <NavLink className={styles.links} to="ingredients">Margins</NavLink>
                </li>

                <li className={styles.navbutton}>
                    <button className={styles.logoutbutton} onClick={logout}>Log Out</button>
                </li>
            </ul>)}
        </div>

        </>
    )
}
export default Nav;
