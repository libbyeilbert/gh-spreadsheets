import { useParams } from "react-router-dom";
import React, { useState, useEffect, useRef } from 'react';
import Popup from 'reactjs-popup';
import { useRecipeContext } from './RecipeContext';

function Cocktail() {
    const { updateRecipeName, fetchRecipes, recipes } = useRecipeContext();
    const { id } = useParams();
    const amountInputRef = useRef();
    const [name, setName] = useState('');
    const [cost, setCost] = useState('');
    const [price, setPrice] = useState('');
    const [yieldAmount, setYieldAmount] = useState('');
    const [yieldUnit, setYieldUnit] = useState('');
    const [ingredient, setIngredient] = useState('');
    const [amount, setAmount] = useState('');
    const [unit, setUnit] = useState('');
    const [ingredients, setIngredients] = useState([]);
    const [editedAmount, setEditedAmount] = useState('');
    const [editedUnit, setEditedUnit] = useState('');
    const [editedIngredient, setEditedIngredient] = useState('');
    const [editedIngredientId, setEditedIngredientId] = useState('');
    const [chosenIngredient, setChosenIngredient] = useState('');

    const handleNameChange = event => {
        setName(event.target.value);
    };

    const handleCostChange = event => {
        setCost(event.target.value);
    };

    const handlePriceChange = event => {
        setPrice(event.target.value);
    };

    const handleYieldAmountChange = event => {
        setYieldAmount(event.target.value);
    };

    const handleYieldUnitChange = event => {
        setYieldUnit(event.target.value);
    };

    const editAmount = event => {
        setEditedAmount(event.target.value);
    };

    const editUnit = event => {
        setEditedUnit(event.target.value);
    };

    const editName = event => {
        setEditedIngredient(event.target.value);
    };

    const handleIngredientChange = event => {
        setIngredient(event.target.value);
    };
    const handleAmountChange = event => {
        setAmount(event.target.value);
    };
    const handleUnitChange = event => {
        setUnit(event.target.value);
    };

    const fetchIngredients  = async () => {
        const url = 'http://localhost:8000/api/ingredients';
        const response = await fetch(url);
        if (response.ok) {
            const ingredientsData = await response.json();
            setIngredients(ingredientsData);
        }
    }

    const handleDeleteRecipe = (recipeId) => {
    const url = `http://localhost:8000/recipes/${recipeId}`;
    fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application.json',
        },
    })
    .then(response => {
        if (response.ok) {
            window.location.href = 'http://localhost:3000/cocktails';
        }
    })

}
    const handleChooseIngredient = async () => {
        const url = `http://localhost:8000/api/ingredients/${ingredient}`;
        const response = await fetch(url);
        if (response.ok) {
            setChosenIngredient(ingredient.name);
            setUnit(ingredient.cost_unit);
        }
    }

    const handleAddIngredient = async event => {
        event.preventDefault();
        const data = {}
        data.name = ingredient;
        data.amount = amount;
        data.unit = unit;
        data.recipe = id;


        const versionUrl = 'http://localhost:8000/ingredients';
        const versionFetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const versionResponse = await fetch(versionUrl, versionFetchConfig);
        if (versionResponse.ok) {
            fetchData();
            setAmount('');
            setUnit('');
            setIngredient('');
            fetchIngredients();
            amountInputRef.current.focus();

        };
    };



    const saveIngredient = async ingredientId => {

        const data = {};
        data.amount = parseFloat(editedAmount);
        data.unit = editedUnit;
        data.name = editedIngredient;
        data.recipe = id;
        const url = `http://localhost:8000/ingredients/${ingredientId}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {}

    }




    const saveRecipeName = async event => {
        event.preventDefault();
        const data = {};
        data.name = name;

        const url = `http://localhost:8000/recipes/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            // updateRecipeName(recipe.id, recipe);
            fetchRecipes();
        }
    }

    const editIngredient = selectedIngredient => {
        setEditedIngredientId(selectedIngredient.id);
        setEditedIngredient(selectedIngredient.name);
        setEditedAmount(selectedIngredient.amount);
        setEditedUnit(selectedIngredient.unit);
    };

    const handleDelete = (ingredientId) => {
        const url = `http://localhost:8000/ingredients/${ingredientId}`;
        fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application.json',
            },
        })
        .then(response => {
            if (response.ok) {
                window.location.reload();
            }
        })

    }
    const fetchData = async () => {
        const recipeUrl = `http://localhost:8000/recipes/${id}`;
        const response = await fetch(recipeUrl);
        if (response.ok) {
            const data = await response.json();
            setName(data.name);
            setYieldAmount(data.yield_amount);
            setYieldUnit(data.yield_unit);
            setCost(data.cost)
        };
    };


    useEffect(() => {
        fetchData();
        fetchIngredients();
        fetchRecipes();
        amountInputRef.current.focus();
    }, []);


    return (
        <>
            <h1>{name}</h1>
            <Popup trigger={
        <button>Edit recipe name</button>
        } position="right center">
    <div>
        <form onSubmit={saveRecipeName} id="recipe-name-change-form">
    <input value={name} onChange={handleNameChange} placeholder="recipe" type="text" id="recipe" />
    <button >Save</button> </form>

    </div>
  </Popup>
  <button onClick={() => handleDeleteRecipe(id)}>Delete recipe</button>
  <h3>Add an ingredient:</h3>
        <form onSubmit={handleChooseIngredient} id="add-ingredient-form">
       <select onChange={handleIngredientChange} id="ingredient-add">
        <option value="">Choose an ingredient</option>
        {ingredients.map(ingredient => {
            return (
                <option key={ingredient.id} value={ingredient.id}>
                    {ingredient.name}
                </option>            );
        })}
        </select>


                <table>
                    <tbody>
                        <tr>
                    <td>
                    <label htmlFor="amount"></label>
                    <input ref={amountInputRef} value={amount} onChange={handleAmountChange} placeholder="amount" type="text" id="amount" />
                    </td>
                    <td>{unit}</td>
                    <td>{chosenIngredient}</td>
                    </tr>
                    </tbody>
                </table><button>Add</button>
            </form>
            <table>
        <tbody>
    {ingredients.map(ingredient => {
        const isEditing = editedIngredientId === ingredient.id;

        return (
            <React.Fragment key={ingredient.id}>
                <tr>
                    <td>{ingredient.amount}</td>

                    <td>
                        <button

                            onClick={() => {
                                editIngredient(ingredient);
                            }}
                        >
                            Edit ingredient
                        </button>
                    </td>
                    <td>
                        <button

                            onClick={() => handleDelete(ingredient.id)}
                        >
                            Delete ingredient
                        </button>
                    </td>

                </tr>
                {isEditing && (
                    <tr>
                        <td colSpan="6">
                            <form>
                                <input
                                    value={editedAmount}
                                    onChange={editAmount}
                                    placeholder="amount"
                                    type="text"
                                    id="editAmount"
                                />
                                <input

                                    value={editedUnit}
                                    onChange={editUnit}
                                    placeholder="unit"
                                    type="text"
                                    id="editUnit"
                                />
                                <input
                                    value={editedIngredient}
                                    onChange={editName}
                                    placeholder="ingredient"
                                    type="text"
                                    id="editName"
                                />
                                <button onClick={() => saveIngredient(ingredient.id)}>Save</button>
                                <button onClick={() => {
                                    setEditedAmount('');
                                    setEditedUnit('');
                                    setEditedIngredient('');
                                }}>Cancel</button>
                            </form>
                        </td>
                    </tr>
                )}
            </React.Fragment>
        );
    })}
</tbody>
</table>





    <p>
    <a href={`http://localhost:3000/cost/${id}`}>Costs →</a>
    </p>
    <p>
    <a href={`http://localhost:3000/calculate/${id}`}>Batch calculations →</a>
    </p>
        </>
    );
};

export default Cocktail;
