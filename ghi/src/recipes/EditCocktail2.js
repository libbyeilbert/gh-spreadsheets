import {useParams } from 'react-router-dom';
import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from './RecipeContext';
import styles from './recipes.module.css';

function EditCocktail() {
    const ref = useRef();
    const inputRef = useRef();
    const { id } = useParams();
    const [recipeName, setRecipeName] = useState('');
    const [ingredientId, setIngredientId] = useState('');
    const [newIngredientAmount, setNewIngredientAmount] = useState('');
    const [recipeCost, setRecipeCost] = useState(0);
    const [addedTwoPercent, setAddedTwoPercent] = useState(false)
    const [manualAddCost, setManualAddCost] = useState(0);
    const [actualPrice, setActualPrice] = useState(0);
    const [drinksPerKeg, setDrinksPerKeg] = useState(1);
    const [threePercentLoss, setThreePercentLoss] = useState(1);
    const [isKegged, setIsKegged] = useState(false);
    const [runningTotal, setRunningTotal] = useState(0);
    const [profit, setProfit] = useState(0);
    const [margin, setMargin] = useState(0);
    const [pourCost, setPourCost] = useState(0);
    const [suggestedRetail, setSuggestedRetail] = useState(0);
    const [marginCalculate, setMarginCalculate] = useState(0);
    const { fetchIngredients, ingredients} = useRecipeContext();
    const [recipeIngredients, setRecipeIngredients] = useState([]);

    const chooseIngredient = event => {
        setIngredientId(event.target.value);
    };

    const handleAmountChange = event => {
        setNewIngredientAmount(event.target.value);
    }

    const handleManualAddCostChange = event => {
        setManualAddCost(event.target.value);
    };

    const handleDrinksPerKegChange = event => {
        setDrinksPerKeg(event.target.value);
        setThreePercentLoss((event.target.value) * .97)
    };

    const handleIsKeggedChange = event => {
        setIsKegged(!isKegged);
    };

    const handleActualPriceChange = event => {
        setActualPrice(event.target.value);
    };

    const handleMarginCalculateChange = event => {
        setMarginCalculate(event.target.value);
    };

    const calculateSuggestedRetail = async event => {
        event.preventDefault();
        setSuggestedRetail(1.09375 * recipeCost /((100-marginCalculate)/100));
    };

    const createIngredientVersion = async event => {
        event.preventDefault();
        const data = {}
        data.ingredient = ingredientId;
        data.amount = newIngredientAmount;
        data.recipe = id;

        const url = 'http://localhost:8000/ingredients';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setNewIngredientAmount('');
            fetchRecipeIngredients();
            fetchRecipeCost();
            inputRef.current.focus();
            ref.current.value = '';
        }
    }


    const deleteIngredientVersion = (ingredientId) => {
        const url = `http://localhost:8000/ingredients/${ingredientId}`;
        fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application.json',
            },
        })
        .then(response => {
            if (response.ok) {
                window.location.reload();
            }
        })
    };

    const addTwoPercent = async event => {
        event.preventDefault();
        setRunningTotal(runningTotal * 1.02);
        setAddedTwoPercent(true);
    };

    const undoAddTwoPercent = async event => {
        event.preventDefault();
        setRunningTotal(runningTotal / 1.02);
        setAddedTwoPercent(false);
    };

    const manuallyAddToCost = async event => {
        event.preventDefault();
        setRunningTotal(runningTotal + parseFloat(manualAddCost));
    };

    const updateCost = async event => {
        event.preventDefault();
        setRecipeCost(runningTotal/threePercentLoss);
    }

    const saveCost = async event => {
        event.preventDefault();
        const url = `http://localhost:8000/recipes/${id}`;
        const data = {};
        data.name = recipeName;
        data.cost = recipeCost;
        data.price = actualPrice;

        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchRecipe();
            fetchMarginsData();
        };
    };

    const fetchRecipe = async () => {
        const url = `http://localhost:8000/recipes/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setRecipeName(data.name);
            setRecipeCost(data.cost || 0);
            setActualPrice(data.price || 0);
        };
    };

    const fetchRecipeCost = async () => {
        const url = `http://localhost:8000/recipes/${id}/cost`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setRecipeCost(data);
            setRunningTotal(data);
        };
    };


    const fetchRecipeIngredients = async () => {
        const url = `http://localhost:8000/ingredients/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setRecipeIngredients(data);
        };
    };

    const fetchMarginsData = async () => {
        const url = `http://localhost:8000/margins/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setProfit(data.profit || 0);
            setMargin(100 * (((actualPrice * 0.90625)-recipeCost) / (actualPrice * 0.90625)) || 0);
            setPourCost(100 * recipeCost / (actualPrice * 0.90625) || 0);
        };
    };

    useEffect(() => {
        fetchRecipe();
        fetchIngredients();
        fetchRecipeIngredients();
        fetchMarginsData();
        fetchRecipeCost();
        inputRef.current.focus();
    }, []);

    return (
        <div className={styles.container}>
            <div className={styles.newrecipecontainer}>
        <h2>{recipeName}</h2>
        <h3>Add ingredients:</h3>
        <form onSubmit={createIngredientVersion} id="add-ingredient">

            <label htmlFor="amount"></label>
            <input ref={inputRef} style={{ margin: '5px 5px', width: '50px' }} required value={newIngredientAmount} onChange={handleAmountChange} placeholder="amount" type="text" id="amount" />
            oz
            <select ref={ref} style={{ margin: '5px 0px 0px 5px' }} required onChange={chooseIngredient} id="all-ingredients">
                <option value="">Choose an ingredient</option>
                {ingredients.map(ingredient => {
                    return (
                        <option key={ingredient.id} value={ingredient.id}>
                            {ingredient.name}
                        </option>
                    );
                })};
            </select>
            <button style={{ margin: '5px 5px 0px 5px' }}>Add</button>
        </form>

        <table>
            <tbody>
                {recipeIngredients.map(ingredient => {
                    return (
                        <tr key={ingredient.version_id}>
                            <td>{ingredient.amount} oz</td>
                            <td>{ingredient.name}</td>
                            <td>${(ingredient.cost).toFixed(2)}</td>
                            <td>
                        <button

                            onClick={() => deleteIngredientVersion(ingredient.version_id)}
                        >
                            Remove
                        </button>
                    </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

        <div className={styles.editcostcontainer}>
            <form onSubmit={manuallyAddToCost} id="manual-add-cost">
                <label htmlFor="add-cost">Manually add cost:</label>
                <input style={{ margin: '5px 5px', width: '50px' }} value={manualAddCost} onChange={handleManualAddCostChange} placeholder="$" type="number" id="add-cost" />
                <button>Add</button>
            </form>
        </div>
        {!addedTwoPercent && (<button onClick={addTwoPercent}>Add 2%?</button>)}
        {addedTwoPercent && (<button onClick={undoAddTwoPercent}>Undo add 2%</button>)}
        <div>
            <button onClick={handleIsKeggedChange}>Kegged cocktail?</button>
            {isKegged && (<div>
        Drinks per keg:
        <input value={drinksPerKeg} onChange={handleDrinksPerKegChange} type="number" id="drinks-per-keg" />
        After 3% loss: {threePercentLoss}</div>)}
        </div>
        </div>
<div className={styles.costcontainer}>
        <h2>Cost per cocktail: ${runningTotal.toFixed(2)}</h2>
        <button onClick={updateCost}>Update</button>
        <div>
        Price: <input value={actualPrice} onChange={handleActualPriceChange} type="number" id="price" />
        </div>
        <div>
        <button onClick={saveCost}>Save recipe info</button>
        </div>
        Profit: ${profit.toFixed(2)}
        Margin: {margin.toFixed(2)}%
        Pour cost: {pourCost.toFixed(2)}%
        <div>
            Margin:
            <input value={marginCalculate} onChange={handleMarginCalculateChange} type="number" id="margin" />%
            <button onClick={calculateSuggestedRetail}>Calculate</button>
            Suggested Retail: ${suggestedRetail.toFixed(2)}
        </div>
        </div>
        </div>
    );
};
export default EditCocktail;
