import React, { useState, useEffect, useRef } from 'react';
import { useRecipeContext } from './RecipeContext';
import styles from './recipes.module.css';
import jwtDecode from 'jwt-decode';

function Cocktails() {
    const token = localStorage.getItem('token');
    const { fetchRecipes, recipes } = useRecipeContext();
    const inputRef = useRef();
    const [name, setName] = useState('');
    const decodedToken = jwtDecode(token);
    const userId = decodedToken.id;



    const handleNameChange = event => {
        setName(event.target.value);
    };



    const handleSubmit = async event => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.user_id = userId;
        const url = 'http://localhost:8000/recipes';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json",
                'Authorization': `Bearer ${token}`
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            fetchRecipes();
            setName('');
            inputRef.current.focus();
        };
    };

    useEffect(() => {
        inputRef.current.focus();
        fetchRecipes();
    }, []);
    return (
        <>
        <div className={styles.container}>
        <div className={styles.newrecipecontainer}>
        New Cocktail:
        <form onSubmit={handleSubmit} id="cocktail-form">
  <label htmlFor="name"></label> {/* Added label for better accessibility */}
  <div>
    <input
      ref={inputRef}
      value={name}
      onChange={handleNameChange}
      placeholder="Name"
      type="text"
      id="name"
style={{ margin: '5px 0' }}    />
  </div>

  <button>Save</button>
</form>
        </div>
        <div className={styles.recipelistcontainer}>
        <ul className={styles.recipelist}>
            {recipes.map(recipe => {
                const recipeUrl = `http://localhost:3000/cocktails/${recipe.id}`
                return (
                    <li className={styles.listitem} key={recipe.id}>
                        <a className={styles.links} href={recipeUrl}>{recipe.name}</a>
                    </li>
                )
            })}
        </ul>
        </div>
        </div>
        </>
    );
 };
 export default Cocktails;
