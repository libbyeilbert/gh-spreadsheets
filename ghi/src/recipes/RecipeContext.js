import { createContext, useContext, useState} from 'react';

const RecipeContext = createContext();

export function useRecipeContext() {
    return useContext(RecipeContext);
}

export function RecipeProvider({ children }) {
    const [recipes, setRecipes] = useState([]);
    const [ingredients, setIngredients] = useState([]);



    const updateRecipeName = (recipeId, newName) => {
        setRecipes(prevRecipes => {
            return prevRecipes.map(recipe => {
                if (recipe.id ===recipeId) {
                    return { ...recipe, name: newName };
                }
                return recipe
            });
        });
    };
    const fetchRecipes = async () => {
        const token = localStorage.getItem('token');
        const url = "http://localhost:8000/recipes/";
        const fetchConfig = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const recipesData = await response.json();
            setRecipes(recipesData);
        }
    };
    const fetchIngredients = async () => {
        const token = localStorage.getItem('token');
        const url = 'http://localhost:8000/api/ingredients';
        const fetchConfig = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const ingredientsData = await response.json();
            setIngredients(ingredientsData);
        }
    }

    return (
        <RecipeContext.Provider value={{ recipes, ingredients, updateRecipeName, fetchRecipes, fetchIngredients }}>
            {children}
        </RecipeContext.Provider>
    );


}
