import React, { useState, useEffect, useRef } from 'react';
import { useNavigate  } from 'react-router-dom';
import styles from './login.module.css';


function Signup() {
    const inputRef = useRef();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordCheck, setPasswordCheck] = useState('');
    const navigate = useNavigate();
    const [error, setError] = useState(null);

    const handleEmailChange = event => {
        setEmail(event.target.value);
    };
    const handlePasswordChange = event => {
        setPassword(event.target.value);
    };
    const handlePasswordCheckChange = event => {
        setPasswordCheck(event.target.value);
    };

    const handleSubmit = async event => {
        event.preventDefault();
        if (password == passwordCheck) {
        const data = {};
        data.email = email;
        data.password = password;

        const url = 'http://localhost:8000/signup';
        const fetchConfig = {
            method: "post",
            credentials: 'include',
            mode: 'cors',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.status == 422) {
            setError('Please enter a valid email.')
        }
         if (response.status == 400) {
            setError('Account already exists with that email.')
         }
        if (response.ok) {
            const loginUrl = 'http://localhost:8000/login';
            const loginFetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const loginResponse = await fetch(loginUrl, loginFetchConfig);
            const loginData = await loginResponse.json();
            const token = loginData.access_token;
            localStorage.setItem('token', token);
            if (loginResponse.ok) {
                navigate('/cocktails');
            };
        };
    }
    else {
        setError('Passwords do not match!')
    };
    };

    useEffect(() => {
        inputRef.current.focus();
    }, []);
    return (
        <>
        <div className={styles.container}>
            <h1>Sign up</h1>
        <form onSubmit={handleSubmit} id="signup-form">
            <div>
            <label htmlFor="email">Email: </label>
            <input required style={{ margin: '5px 0' }} ref={inputRef} value={email} onChange={handleEmailChange} placeholder="" type="text" id="email" />
            </div>
            <div>
            <label htmlFor="password">Password: </label>
            <input required style={{ margin: '5px 0' }} value={password} onChange={handlePasswordChange} placeholder='' type="password" id="password" />
            </div>
            <div>
                <label htmlFor="password-check">Confirm Password: </label>
                <input required style={{ margin: '5px 0'}} value={passwordCheck} onChange={handlePasswordCheckChange} placeholder='' type="password" id='password-check' />
            </div>
            {error && <p>{error}</p>}
            <div>
            <button>Create account</button>
            </div>
        </form>
        </div>
        <div className={styles.signupcontainer}>
        Have an account? <button onClick={() => navigate('/')}>Log in</button>
        </div>
        </>
    )
};

export default Signup;
