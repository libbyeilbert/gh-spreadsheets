import React, { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './login.module.css';

function Login() {
    const inputRef = useRef();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const [wrongPasswordError, setWrongPasswordError] = useState(null);

    const handleEmailChange = event => {
        setEmail(event.target.value);
    };
    const handlePasswordChange = event => {
        setPassword(event.target.value);
    };

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {};
        data.email = email;
        data.password = password;

        const url = 'http://localhost:8000/login';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        const responseData = await response.json();
        const token = responseData.access_token;
        localStorage.setItem('token', token);
        if(response.status == 404) {
            setWrongPasswordError("No account exists with that email!")
        }
        if (response.status == 401) {
            setWrongPasswordError("Wrong password!");
        }
        if (response.ok) {
            navigate('/cocktails');
        };
    };

    useEffect(() => {
        inputRef.current.focus();
    }, []);

    return (
        <>
        <div className={styles.container}>
            <h1>Log in</h1>
        <form onSubmit={handleSubmit} id="login-form">
            <div>
            <label htmlFor="email">Email: </label>
            <input required style={{ margin: '5px 0' }} ref={inputRef} value={email} onChange={handleEmailChange} placeholder="" type="text" id="email" />
            </div>
            <div>
            <label htmlFor="password">Password: </label>
            <input required style={{ margin: '5px 0' }} value={password} onChange={handlePasswordChange} placeholder='' type="password" id="password" />
            {wrongPasswordError && <p>{wrongPasswordError}</p>}
            </div>
            <div>
            <button>Log in</button>
            </div>
        </form>
        </div>
        <div className={styles.signupcontainer}>
        No account yet? <button onClick={() => navigate('/signup')}>Sign up!</button>
        </div>
        </>
    )
};

export default Login;
