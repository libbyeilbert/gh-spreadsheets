import { BrowserRouter, Routes, Route } from "react-router-dom";
import React from "react";
import { RecipeProvider } from './recipes/RecipeContext';
import Nav from "./Nav";
import Ingredient from "./ingredients/Ingredients";
import EditIngredient from "./ingredients/EditIngredient";
import Cocktails from "./recipes/Cocktails";
import Cocktail from "./recipes/EditCocktail";
import EditCocktail from "./recipes/EditCocktail2";
import Signup from "./users/Signup";
import Login from "./users/Login";


function App() {

  return (
    <RecipeProvider>
    <BrowserRouter>
    <Nav />
      <div>
        <Routes>
          <Route path="" element={<Login />} />
          <Route path="signup" element={<Signup />} />
          <Route path="cocktails/:id" element={<EditCocktail />} />
          <Route path="cocktails" element={<Cocktails />} />
          <Route path="ingredients" element={<Ingredient />} />
          <Route path="ingredients/:id" element={<EditIngredient />} />
        </Routes>
      </div>
    </BrowserRouter>
    </RecipeProvider>
  )
};

export default App;
