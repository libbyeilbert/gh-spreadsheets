steps = [
    [
        """
        CREATE TABLE "user" (
            id SERIAL PRIMARY KEY NOT NULL,
            email TEXT NOT NULL,
            password BYTEA NOT NULL
        );
        """,
        """
        DROP TABLE "user";
        """,
    ],
    [
        """
        CREATE TABLE recipe (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            cost NUMERIC(10, 2),
            price NUMERIC(10,2),
            user_id INTEGER REFERENCES "user"("id")
        );
        """,
        """
        DROP TABLE recipes;
        """,
    ],
    [
        """
        CREATE TABLE ingredient (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            cost_per_oz NUMERIC(10, 2),
            source VARCHAR(100),
            par_amount NUMERIC(10, 6),
            par_unit VARCHAR(50),
            user_id INTEGER REFERENCES "user"("id")
        );
        """,
        """
        DROP TABLE ingredients;
        """,
    ],
    [
        """
        CREATE TABLE ingredient_version (
            id SERIAL PRIMARY KEY NOT NULL,
            ingredient INTEGER REFERENCES ingredient("id") ON DELETE CASCADE,
            amount NUMERIC(20, 6),
            recipe INTEGER REFERENCES recipe("id") ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE ingredient_version;
        """,
    ],
]
