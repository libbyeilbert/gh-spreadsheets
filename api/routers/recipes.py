from fastapi import APIRouter, Depends, HTTPException
from typing import List
from queries.users import User, UserQueries
from queries.recipes import (
    RecipeOut,
    RecipeIn,
    RecipeQueries,
)
from fastapi.security import OAuth2PasswordBearer
import jwt
import os
from jwt.exceptions import PyJWTError


router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="http://localhost:8000/login")
JWT_SECRET_KEY = os.environ["SECRET_KEY"]


def get_current_user(
    token: str = Depends(oauth2_scheme), queries: UserQueries = Depends()
):
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=["HS256"])
        username = payload.get("email")
        print(payload)
        if not username:
            raise HTTPException(status_code=401, detail="Invalid username")

    except PyJWTError:
        raise HTTPException(status_code=401, detail="Invalid token")

    # Fetch user from DB
    return queries.get_user(username)


@router.get("/recipes", response_model=List[RecipeOut])
def get_recipes(
    user: User = Depends(get_current_user), queries: RecipeQueries = Depends()
):
    userId = user.id
    return queries.get_recipes(userId)


@router.get("/recipes/{recipe_id}", response_model=RecipeOut)
def get_recipe(recipe_id: int, queries: RecipeQueries = Depends()):
    return queries.get_recipe(recipe_id)


@router.post("/recipes", response_model=RecipeOut)
def create_recipe(
    recipe: RecipeIn,
    user: User = Depends(get_current_user),
    queries: RecipeQueries = Depends(),
):
    userId = user.id
    return queries.create_recipe(recipe, userId)


@router.put("/recipes/{recipe_id}", response_model=RecipeOut)
def edit_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    queries: RecipeQueries = Depends(),
):
    return queries.edit_recipe(recipe_id, recipe)


@router.delete("/recipes/{recipe_id}", response_model=bool)
def delete_recipe(recipe_id: int, queries: RecipeQueries = Depends()):
    queries.delete_recipe(recipe_id)
    return True


@router.get("/recipes/{recipe_id}/cost", response_model=float)
def get_recipe_cost(recipe_id: int, queries: RecipeQueries = Depends()):
    return queries.get_recipe_cost(recipe_id)
