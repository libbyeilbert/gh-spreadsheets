from fastapi import APIRouter, Depends, HTTPException
from typing import List
from queries.ingredients import (
    IngredientOut,
    IngredientIn,
    IngredientQueries,
    IngredientVersionIn,
    IngredientVersionOut,
    AllDataOut,
)
from queries.users import UserQueries, User
from fastapi.security import OAuth2PasswordBearer
import jwt
import os
from jwt.exceptions import PyJWTError

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="http://localhost:8000/login")
JWT_SECRET_KEY = os.environ["SECRET_KEY"]


def get_current_user(
    token: str = Depends(oauth2_scheme), queries: UserQueries = Depends()
):
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=["HS256"])
        username = payload.get("email")
        print(payload)
        if not username:
            raise HTTPException(status_code=401, detail="Invalid username")

    except PyJWTError:
        raise HTTPException(status_code=401, detail="Invalid token")

    # Fetch user from DB
    return queries.get_user(username)


@router.get("/ingredients/{recipe_id}", response_model=List[AllDataOut])
def get_all_ingredient_data(
    recipe_id: int, queries: IngredientQueries = Depends()
):
    return queries.get_all_ingredient_data(recipe_id)


@router.get("/api/ingredients", response_model=List[IngredientOut])
def get_ingredients(
    user: User = Depends(get_current_user),
    queries: IngredientQueries = Depends(),
):
    userId = user.id
    return queries.get_ingredients(userId)


@router.get("/api/ingredients/{ingredient_id}", response_model=IngredientOut)
def get_ingredient(ingredient_id: int, queries: IngredientQueries = Depends()):
    return queries.get_ingredient(ingredient_id)


@router.post("/api/ingredients", response_model=IngredientOut)
def create_ingredient(
    ingredient: IngredientIn,
    user: User = Depends(get_current_user),
    queries: IngredientQueries = Depends(),
):
    return queries.create_ingredient(ingredient)


@router.put("/api/ingredients/{ingredient_id}", response_model=IngredientOut)
def edit_ingredient(
    ingredient_id: int,
    ingredient: IngredientIn,
    queries: IngredientQueries = Depends(),
):
    return queries.edit_ingredient(ingredient_id, ingredient)


@router.delete("/api/ingredients/{ingredient_id}", response_model=bool)
def delete_ingredient(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    queries.delete(ingredient_id)
    return True


@router.get("/ingredients", response_model=List[IngredientVersionOut])
def get_ingredient_versions(queries: IngredientQueries = Depends()):
    return queries.get_ingredient_versions()


@router.get(
    "/ingredients/{ingredient_id}", response_model=IngredientVersionOut
)
def get_ingredient_version(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    return queries.get_ingredient_version(ingredient_id)


@router.post("/ingredients", response_model=IngredientVersionOut)
def create_ingredient_version(
    ingredient: IngredientVersionIn,
    queries: IngredientQueries = Depends(),
):
    return queries.create_ingredient_version(ingredient)


@router.put(
    "/ingredients/{ingredient_id}", response_model=IngredientVersionOut
)
def edit_ingredient_version(
    ingredient_id: int,
    ingredient: IngredientVersionIn,
    queries: IngredientQueries = Depends(),
):
    return queries.edit_ingredient_version(ingredient_id, ingredient)


@router.delete("/ingredients/{ingredient_id}", response_model=bool)
def delete_ingredient_version(
    ingredient_id: int, queries: IngredientQueries = Depends()
):
    queries.delete_version(ingredient_id)
    return True
