from fastapi import APIRouter, Depends
from typing import List
from queries.margins import MarginQueries, Margin

router = APIRouter()


@router.get("/margins", response_model=List[Margin])
def get_margins(queries: MarginQueries = Depends()):
    return queries.get_margins()


@router.get("/margins/{recipe_id}", response_model=Margin)
def get_recipe_margin(recipe_id: int, queries: MarginQueries = Depends()):
    return queries.get_margin(recipe_id)
