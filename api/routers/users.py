from fastapi import APIRouter, Depends, Body, HTTPException, status
from queries.users import (
    User,
    UserQueries,
    UserSchema,
    CreateUserSchema,
    UserLoginSchema,
    UserNotFoundError,
)
from typing import Dict


router = APIRouter()


@router.post("/signup", response_model=UserSchema)
def signup(user: CreateUserSchema = Body(), queries: UserQueries = Depends()):
    try:
        queries.get_user(email=user.email)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Email already exists",
        )

    except UserNotFoundError:
        user.hashed_password = User.hash_password(user.hashed_password)
        new_user = queries.create_user(user=user)
        return new_user


@router.post("/login", response_model=Dict)
def login(
    payload: UserLoginSchema = Body(),
    queries: UserQueries = Depends(),
):
    try:
        user: User = queries.get_user(email=payload.email)
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No such user",
        )
    is_validated: bool = user.validate_password(payload.password)
    if not is_validated:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Wrong password",
        )
    return user.generate_token()
