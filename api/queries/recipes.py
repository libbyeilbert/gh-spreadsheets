from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import Optional, List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class RecipeIn(BaseModel):
    name: str
    cost: Optional[float]
    price: Optional[float]
    user_id: int


class RecipeOut(BaseModel):
    id: int
    name: str
    cost: Optional[float]
    price: Optional[float]
    user_id: int


class RecipeQueries:
    def get_recipes(self, userId: int) -> List[RecipeOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT *
                    FROM recipe
                    WHERE user_id = %s
                    """,
                    [userId],
                )
                result = db.fetchall()
                return [self.record_to_recipe_out(record) for record in result]

    def get_recipe(self, recipe_id: int) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT *
                    FROM recipe
                    WHERE id = %s
                    """,
                    [recipe_id],
                )
                result = db.fetchone()
                return self.record_to_recipe_out(result)

    def create_recipe(self, recipe: RecipeIn, userId: int) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO recipe (
                    name, cost, price, user_id
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id, name, cost, price, user_id
                    """,
                    [recipe.name, recipe.cost, recipe.price, userId],
                )
                id = result.fetchone()[0]
                return self.recipe_in_to_out(id, recipe)

    def edit_recipe(self, id: int, recipe: RecipeIn) -> RecipeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE recipe
                    SET name = %s,
                        cost = %s,
                        price = %s
                    WHERE id = %s
                    """,
                    [
                        recipe.name,
                        recipe.cost,
                        recipe.price,
                        id,
                    ],
                )
                return self.recipe_in_to_out(id, recipe)

    def record_to_recipe_out(self, record):
        return RecipeOut(
            id=record[0],
            name=record[1],
            cost=record[2],
            price=record[3],
            user_id=record[4],
        )

    def recipe_in_to_out(self, id: int, recipe: RecipeIn):
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)

    def delete_recipe(self, recipe_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM recipe
                    WHERE id = %s
                    """,
                    [recipe_id],
                )
                return True

    def get_recipe_cost(self, id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                        SELECT sum(i.cost_per_oz * iv.amount) as total
                        FROM ingredient as i
                        INNER JOIN ingredient_version as iv
                        ON i.id = iv.ingredient
                        WHERE iv.recipe = %s
                        """,
                    [id],
                )
                result = db.fetchone()
                total = result[0] if result else None

        return total
