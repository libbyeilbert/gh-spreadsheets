from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import List, Optional

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class Margin(BaseModel):
    id: int
    name: str
    total: Optional[float]
    price: Optional[float]
    profit: Optional[float]
    margin: Optional[float]
    pour_cost: Optional[float]
    suggested_retail: Optional[float]


class MarginQueries:
    def get_margins(self) -> List[Margin]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT r.id,
                    r.name,
                    r.cost,
                    r.price,
                    (r.price * 0.90625)-r.cost as profit,
                    100 * (((r.price * 0.90625)-r.cost) / (r.price * 0.90625)) as margin,
                    100 * r.cost / (r.price * 0.90625) as pour_cost,
                    1.09375 * r.cost / .15 as suggested_retail

                    FROM recipe as r
                    WHERE r.price <> 0
                    """,
                )
                result = db.fetchall()
                return [self.record_to_margin_out(record) for record in result]

    def get_margin(self, id: int) -> Margin:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT r.id,
                    r.name,
                    r.cost,
                    r.price,
                    (r.price * 0.90625)-r.cost as profit,
                    100 * (((r.price * 0.90625)-r.cost) / (r.price * 0.90625)) as margin,
                    100 * r.cost / (r.price * 0.90625) as pour_cost,
                    1.09375 * r.cost / .15 as suggested_retail

                    FROM recipe as r
                    WHERE r.id = %s
                    """,
                    [id],
                )
                result = db.fetchone()
                return self.record_to_margin_out(result)

    def record_to_margin_out(self, record):
        return Margin(
            id=record[0],
            name=record[1],
            total=record[2],
            price=record[3],
            profit=record[4],
            margin=record[5],
            pour_cost=record[6],
            suggested_retail=record[7],
        )
