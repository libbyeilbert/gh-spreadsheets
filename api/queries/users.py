from pydantic import BaseModel, Field, EmailStr
import os
from psycopg_pool import ConnectionPool
import bcrypt
import jwt

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])
SECRET_KEY = os.environ["SECRET_KEY"]


class User(BaseModel):
    email: EmailStr
    hashed_password: bytes = Field(alias="password")
    id: int

    @staticmethod
    def hash_password(password) -> str:
        """Transforms password from its raw textual form to
        cryptographic hashes
        """
        salt = bcrypt.gensalt()
        hashed_pw = bcrypt.hashpw(password, salt)
        return hashed_pw

    def validate_password(self, password) -> bool:
        """Confirms password validity"""
        encoded_pw = password.encode("utf-8")
        hashed_pw = self.hashed_password
        return bcrypt.checkpw(encoded_pw, hashed_pw)

    def generate_token(self) -> dict:
        """Generate access token for user"""
        payload = {"email": self.email, "id": self.id}
        algorithm = "HS256"  # Specify the algorithm you're using
        access_token = jwt.encode(payload, SECRET_KEY, algorithm=algorithm)
        return {"access_token": access_token}


class UserBaseSchema(BaseModel):
    email: EmailStr


class CreateUserSchema(UserBaseSchema):
    hashed_password: bytes = Field(alias="password")


class UserSchema(UserBaseSchema):
    id: int

    class Config:
        orm_mode = True


class UserLoginSchema(BaseModel):
    email: EmailStr
    password: str


class UserNotFoundError(Exception):
    pass


class UserQueries:
    def create_user(self, user: CreateUserSchema) -> User:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO "user" (
                        email, password
                    )
                    VALUES (%s, %s)
                    RETURNING id, email, password
                    """,
                    [
                        user.email,
                        user.hashed_password,
                    ],
                )
                id = result.fetchone()[0]

                return User(
                    id=id,
                    email=user.email,
                    password=user.hashed_password,
                )

    def get_user(self, email: str) -> User:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, email, password
                    FROM "user"
                    WHERE email = %s
                    """,
                    [email],
                )
                if result.rowcount == 0:
                    raise UserNotFoundError()
                record = result.fetchone()

                return User(id=record[0], email=record[1], password=record[2])
