from pydantic import BaseModel
import os
from psycopg_pool import ConnectionPool
from typing import Optional, List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class IngredientIn(BaseModel):
    name: str
    cost_per_oz: Optional[float]
    source: Optional[str]
    par_amount: Optional[float]
    par_unit: Optional[str]
    user_id: int


class IngredientVersionIn(BaseModel):
    ingredient: int
    amount: Optional[float]
    recipe: Optional[int]


class AllDataOut(BaseModel):
    ingredient_id: int
    version_id: int
    name: str
    cost_amount: Optional[float]
    source: Optional[str]
    par_amount: Optional[float]
    par_unit: Optional[str]
    amount: Optional[float]
    cost: Optional[float]


class IngredientOut(IngredientIn):
    id: int


class IngredientVersionOut(IngredientVersionIn):
    id: int


class IngredientQueries:
    def get_ingredients(self, userId: int) -> List[IngredientOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, cost_per_oz, source, par_amount, par_unit, user_id
                    FROM ingredient
                    WHERE user_id = %s
                    """,
                    [userId],
                )
                result = db.fetchall()
                return [
                    self.record_to_ingredient_out(record) for record in result
                ]

    def get_ingredient(self, ingredient_id: int) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, cost_per_oz, source, par_amount, par_unit, user_id
                    FROM ingredient
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                result = db.fetchone()
                return self.record_to_ingredient_out(result)

    def create_ingredient(
        self, ingredient: IngredientIn
    ) -> IngredientOut | None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO ingredient (
                        name, cost_per_oz, source, par_amount, par_unit, user_id
                    )
                    VALUES (%s, %s, %s, %s, %s, %s)
                    RETURNING id, name, cost_per_oz source, par_amount, par_unit, user_id
                    """,
                    [
                        ingredient.name,
                        ingredient.cost_per_oz,
                        ingredient.source,
                        ingredient.par_amount,
                        ingredient.par_unit,
                        ingredient.user_id,
                    ],
                )

                id = result.fetchone()[0]
                return self.ingredient_in_to_out(id, ingredient)

    def edit_ingredient(
        self, id: int, ingredient: IngredientIn
    ) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE ingredient
                    SET name = %s,
                    cost_per_oz = %s,
                    source = %s,
                    par_amount = %s,
                    par_unit = %s,
                    user_id = %s
                    WHERE id = %s
                    """,
                    [
                        ingredient.name,
                        ingredient.cost_per_oz,
                        ingredient.source,
                        ingredient.par_amount,
                        ingredient.par_unit,
                        ingredient.user_id,
                        id,
                    ],
                )
                return self.ingredient_in_to_out(id, ingredient)

    def record_to_ingredient_out(self, record):
        return IngredientOut(
            id=record[0],
            name=record[1],
            cost_per_oz=record[2],
            source=record[3],
            par_amount=record[4],
            par_unit=record[5],
            user_id=record[6],
        )

    def ingredient_in_to_out(self, id: int, ingredient: IngredientIn):
        old_data = ingredient.dict()
        return IngredientOut(id=id, **old_data)

    def delete(self, ingredient_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM ingredient
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                return True

    def get_ingredient_versions(self) -> List[IngredientVersionOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, ingredient, amount, recipe
                    FROM ingredient_version
                    """
                )
                result = db.fetchall()
                return [
                    self.record_to_version_out(record) for record in result
                ]

    def record_to_all_data_out(self, record):
        return AllDataOut(
            ingredient_id=record[0],
            name=record[1],
            version_id=record[2],
            amount=record[3],
            cost_amount=record[4],
            source=record[5],
            par_amount=record[6],
            par_unit=record[7],
            cost=record[8],
        )

    def get_all_ingredient_data(self, recipe_id: int) -> List[AllDataOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT i.id as ingredient_id, i.name, iv.id as version_id, iv.amount, i.cost_per_oz, i.source, i.par_amount, i.par_unit, iv.amount * i.cost_per_oz as cost
                    FROM ingredient as i
                    INNER JOIN ingredient_version as iv
                    ON i.id = iv.ingredient
                    WHERE iv.recipe = %s
                    """,
                    [recipe_id],
                )
                result = db.fetchall()
                return [
                    self.record_to_all_data_out(record) for record in result
                ]

    def get_ingredient_version(self, version_id: int) -> IngredientVersionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, ingredient, amount, recipe
                    FROM ingredient_version
                    WHERE id = %s
                    """,
                    [version_id],
                )
                result = db.fetchone()
                return self.record_to_version_out(result)

    def create_ingredient_version(
        self, ingredient: IngredientVersionIn
    ) -> IngredientVersionOut | None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO ingredient_version (
                        ingredient, amount, recipe
                    )
                    VALUES (%s, %s, %s)
                    RETURNING id, ingredient, amount, recipe
                    """,
                    [
                        ingredient.ingredient,
                        ingredient.amount,
                        ingredient.recipe,
                    ],
                )

                id = result.fetchone()[0]
                return self.version_in_to_out(id, ingredient)

    def edit_ingredient_version(
        self, id: int, ingredient: IngredientVersionIn
    ) -> IngredientVersionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE ingredient_version
                    SET ingredient = %s,
                    amount = %s,
                    recipe = %s
                    WHERE id = %s
                    """,
                    [
                        ingredient.ingredient,
                        ingredient.amount,
                        ingredient.recipe,
                        id,
                    ],
                )
                return self.version_in_to_out(id, ingredient)

    def record_to_version_out(self, record):
        return IngredientVersionOut(
            id=record[0],
            ingredient=record[1],
            amount=record[2],
            recipe=record[3],
        )

    def version_in_to_out(self, id: int, ingredient: IngredientVersionIn):
        old_data = ingredient.dict()
        return IngredientVersionOut(id=id, **old_data)

    def delete_version(self, ingredient_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM ingredient_version
                    WHERE id = %s
                    """,
                    [ingredient_id],
                )
                return True
